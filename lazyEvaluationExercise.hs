import Data.List

-- NUMBER 1

expr = [x+y | x <- [1..4], y <- [2..4], x > y]
-- This expression takes the first element of list x (which is 1), then adds it to each element of list y
-- that is bigger than itself (none). Then, it moves to the second element (2) and adds it to each element of list y
-- that is bigger than itself (also none). After, it moves to the third element (3) and adds it to each element of 
-- that is bigger than itself (3,4). It then appends the result to a new empty list and moves on to the last element
-- of list x, and repeats the cycle.

-- NUMBER 2
divisor n = [x | x <- [1..], n `mod` x == 0]

-- NUMBER 3
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y <= x] ++ [x] ++ quicksort [y | y <- xs, y > x]

-- NUMBER 4
perm [] = [[]]
perm ls = [ x:xs | x <- ls, xs <- perm(ls \\ [x])]

-- NUMBER 5
primes = sieve[2..10]
    where sieve(x:xs) = x : (sieve[z | z <- xs, z `mod` x /= 0])

-- NUMBER 6
pythaTriple = [(x,y,z) | z <- [1..], y <- [z,z-1..1], x <- [y,y-1..1], x**2 + y**2 == z**2]
