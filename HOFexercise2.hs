import Data.Maybe

-- NUMBER 1A - 6A
hof1A xs = map (+1) xs
-- adds 1 to each element in list xs

hof2A xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)
-- equivalent to [x+y | x <- xs, y <- ys]

hof3A xs = map (+2) (filter (>3) xs)
-- adds 2 to each element in the list with value > 3

hof4A xys = map (\(x,_) -> x+3) xys
-- takes first element of each pair and adds it by 3

hof5A xys = map (\(x,_) -> x+4) (filter(\(x,y) -> x+y < 5) xys)
-- takes first element of each pair (x,y) where x+y < 5, and adds it by 4

hof6A mxs = map (\(Just x) -> x+5) (filter isJust mxs)
-- equivalent to [x+5 | Just x <- mxs]

-- NUMBER 1B - 4B
hof1B xs = [x+3 | x <- xs]
-- equivalent to map(+3) xs

hof2B xs = [x | x <- xs, x > 7]
-- equivalent to filter (> 7) xs

hof3B xs ys = [(x,y) | x <- xs, y <- ys]
-- equivalent to concat (map (\x -> map (\y -> (x,y)) ys) xs)

hof4B xys = [x+y | (x,y) <- xys, x+y > 3]
-- equivalent to filter (>3) (map (\(x,y) -> x+y) xys)
