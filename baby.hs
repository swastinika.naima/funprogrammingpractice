-- function that doubles input
doubleMe x = x + x

-- function that returns sum of the inputs doubled
doubleUs x y = doubleMe x + doubleMe y

-- function that returns double input if input is less than 100
doubleSmallNumber x = if x > 100
                        then x
                        else x * 2

-- function that works the same as doubleSmallNumber and adds 1 to the result
doubleSmallNumber' x = (if x > 100 then x else x * 2) + 1

-- returns the string described
conanO'Brien = "It's a-me! Conan O'Brien!"
