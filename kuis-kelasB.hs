-- NOMOR 1
kpk :: Integral a => a -> a -> a
kpk x y = lcm x y

--Alternatively, without the use of lcm function:
findLCM :: Integral a => a -> a -> a
findLCM y z = head[x | x <- [1..(y*z)], x `mod` y == 0, x `mod` z == 0]

-- NOMOR 2
expr = [(x,y) | x <- [1..3], y <- [1..(2*x)]]
-- This function returns a list of tuples (x,y) where x is from the [1,2,3] and y is [1..(2*x)].
-- First, the function takes the first element of list x (1), and pairs it with each element in list y [1..(2*x)].
-- Because the x is 1, that means 1*2 == 2, so the list y becomes [1,2]. So the produced pair is (1,1),(1,2). These two pairs
-- are then appended to an empty list, so the output so far becomes [(1,1),(1,2)]. It then repeats for each x = 2 and x = 3.

-- NOMOR 4
maxList xs = foldr max 0 xs

-- Alternatively, we can use foldr1 (where we take 2 elements of a list and apply the function to it, then take a 3rd element and apply it to the result, so on)
maxList' :: (Ord a) => [a] -> a
maxList' = foldr1 (\x acc -> if x > acc then x else acc)

-- NOMOR 5: sama dengan soal kelas A

-- NOMOR 6
phytagoras = [(x,y,z) | z <- [1..], y <- [z,z-1..1], x <- [y,y-1..1], x**2 + y**2 == z**2]

-- NOMOR 7
flip' :: (a -> b -> c) -> b -> a -> c
flip' f x y = y `f` x