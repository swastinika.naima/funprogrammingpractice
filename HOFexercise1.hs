-- 1. Exercises from chapter 9-10 of The Craft of Functional Programming

-- Redefining length function using map and sum
length' a = sum(map(const 1) a)
-- NOTE: the function map(const 1) maps the number 1 to each element in a, and then the function sum adds all the 1s, resulting in the length of list.

-- The expression map(+1) (map(+1) xs) iterates through list xs and add all its element(s) by 1, then repeats it
-- for example, if we run map(+1) (map(+1) [5,6,7,8]), it will result in [7,8,9,10].
-- For arbitrary functions f and g, we can execute map f (map g xs) as long as f and g define a function that can be operated by element inside
-- function function (such as (+1), abs, reverse, etc). Otherwise, it would produce an error.

iter :: Integral n => n -> (a -> a) -> a -> a
-- Integral n defines the constraint that n must be either Int or Integral. Input takes 3 parameters: an arbitrary Integral n, a function that maps variable with data type a
-- to another variable with data type a, another variable with data type a (therefore applicable to the function). Function then provides output with data type a.
iter 0 f x = x
iter n f x = iter (n-1) f (f x)
-- Recursively run iter until iter 0, applying function along the way.
-- E.g.: iter 2 (5*) 1 returns iter 1 (5*) (5*1), which returns iter 0 (5*) (5*(5*1)), which returns (5*(5*1))

-- \n -> iter n succ is an anonymous function (nameless function) that will run the iter function, taking 2 parameters: 
-- n and the number we want the successor for.
-- E.g.: (\n -> iter n succ) 3 7 will return 10.

sumOfSquares :: (Eq t, Floating t, Enum t) => t -> t
sumOfSquares 0 = 1
sumOfSquares x = foldr (+) 0 (map(**2) [1..x])

mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]
-- This code will take input in the form of a list, and returns the list itself.
-- First, it takes a list as a parameter. Then, it maps each element of the list into a list containing just the element itself.
-- Afterwards, it appends each list to an empty list, creating the original list.

f :: Int -> Bool
f 0 = False
f x = True

id' x = x

-- (id' . f) x will equal to id' (f x) but (id' . f 10) will produce error
-- (f . id') x will equal to f (id' x) or f x itself but (f . id' 10) will produce error
-- (id' f x) will equal to id' (f x)
-- id' will behave as its general type a -> a in (id' .f ) x and (id' f x)

composeList :: [a -> a] -> (a -> a)
composeList []     = id
composeList (f:fs) = f . composeList fs

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = \x y -> f y x

-- flip' function flips the parameter of the functions
