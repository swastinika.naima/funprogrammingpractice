-- NOMOR 1
maxTiga :: Ord a => a -> a -> a -> a
maxTiga x y z = max x (max y z)

-- NOMOR 2
expr = [(x,y) | x <- [1..4], y <- [2..6], x*2 == y]
-- This function will return a list of tuples (x,y) where x * 2 == y.
-- First, it will take the first element of list x (which is 1) and compare it to the first element of
-- list y (which is 2). Because 1 * 2 == 2, it will then append (1,2) to a new empty list. After, it moves to
-- the 2nd element of list x (2) and compare it to the first element of list y (1). 2 * 2 /= 1, it moves to the 2nd
-- element in list y (3). 2 * 2 /= 3, so it moves to the 3rd element in list y (4). 2 * 2 == 4, so it appends (2,4)
-- to the output list. Algorithm will continue with the next element in list y (3).

-- NOMOR 3
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y <= x] ++ [x] ++ quicksort [y | y <- xs, y > x]

-- NOMOR 4
jumlahList [] = 0
jumlahList xs = foldr (+) 0 xs

-- NOMOR 5
misteri xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)
-- Essentially, this produces a list of pairs (x,y), with x being an element from list xs and y being an element from ys
-- E.g.: misteri [1,2] [4,5]
-- First, it will take the first element of xs (1) and map it to a pair (1,_). It will then map all elements of y to _, producing
-- 3 pairs: (1,4),(1,5). It then appends these 3 pairs into an empty list, creating [(1,4),(1,5)].
-- Second, it will take the second element of xs (2) and repeat the step. Creating (2,4),(2,5),(2,6). The output then becomes [(1,4),(1,5),(2,4),(2,5)]

-- NOMOR 6
primes = sieve[2..10]
    where sieve(x:xs) = x : (sieve[z | z <- xs, z `mod` x /= 0])

-- NOMOR 7
-- Flip function flips the parameter of functions, so the type is:
flip :: (a -> b -> c) -> b -> a -> c
flip f = \x y -> f y x