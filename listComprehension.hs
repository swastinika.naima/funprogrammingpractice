-- replaces each odd number with "BANG" and even number with "BOOM" for range specified
boomBang xs = [if x `mod` 2 == 1 then "BANG" else "BOOM" | x <- xs]

-- returns all combination of adjectives and nouns from list given
combineWords adjectives nouns = [adjective ++ " " ++ noun | adjective <- adjectives, noun <- nouns]

-- function that returns the length of a list
length' xs = sum[1 | _ <- xs]

-- function that removes everything but UpperCase from string/list (strings are a list of chars, so it's basically the same!)
removeNonUpperCase string = [char | char <- string, char `elem` ['A'..'Z']]

-- function that returns all triangles with all the sides between 1 to 10
triangles = [(a,b,c) | a <- [1..10], b <- [1..10], c <- [1..10]]

-- function that returns all pythagorean triangle where c is hypothenuse, b is no larger than c, and a is no larger than b
rightTriangles = [(a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2+b^2 == c^2]

-- function that returns all pythagorean triangle with perimeter 24
rightTriangles' = [(a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2+b^2 == c^2, a+b+c == 24]
