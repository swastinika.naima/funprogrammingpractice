import Data.List 
import qualified Data.Char as Char

-- KUIS 2 KELAS A

-- NOMOR 1
-- Given a list of words, remove from the list all those which contain four or more vowels 
-- and those which have same letter appearing twice (or more) in a row. In addition, 
-- any word containing numbers should have the numbers removed. Note that the number removal 
-- should happen before any other operations so that the subsequent operations can remove the 
-- word if necessary. (source: https://www.fer.unizg.hr/)

notNumber x = x `notElem` "0123456789" -- checks if a char is not an element of the list provided, in this case all numbers
numberRemoval = filter notNumber -- filters a String and removes chars that return False upon notNumber check

isVowel x = x `elem` "aiueoAIUEO" -- checks if a char is an element of the list provided, in this case list of all vowels
countVowel = length . filter isVowel -- filters a String and counts the amount of chars that return True upon isVowel check
-- f = length x
-- g = filter isVowel
-- f(g(input)) = (f . g) input

isThreeOrLessVowels str = countVowel str < 4 -- asserts that there are less than 4 vowels in String

tempFunction = ((filter isThreeOrLessVowels) . (map numberRemoval)) -- removes number from Str, then checks 3 or less vowels
tempFunction2 = filter (isThreeOrLessVowels . numberRemoval) -- doesn't remove number before checking 3 or less vowels

isNotAppearTwiceInARow (a:b:ls) | (a == b) = False
isNotAppearTwiceInARow (a:ls) = isNotAppearTwiceInARow ls
isNotAppearTwiceInARow [] = True
-- Recursive function that checks if there are any letters that appear twice in a row; returns false if yes

weirdFilter :: [String] -> [String]
weirdFilter = filter isNotAppearTwiceInARow . filter isThreeOrLessVowels . map numberRemoval

and' p1 p2 = (\x -> p1 x && p2 x) -- returns true if both functions p1 and p2 return true
weirdFilter' = filter (isNotAppearTwiceInARow `and'` isThreeOrLessVowels) . map numberRemoval

-- NOMOR 2
-- Write a function rotabc that changes a's to b's, b's to c's and c's to a's in a string. 
-- Only lowercase letters are affected. (source: https://www2.cs.arizona.edu/classes/cs372/spring14)

rotabc :: String -> String
rotabc (x:xs) | x == 'a' = 'b' : rotabc xs
              | x == 'b' = 'c' : rotabc xs
              | x == 'c' = 'a' : rotabc xs
              | otherwise  = x : rotabc xs -- results in infinite loop because of non-exhaustive pattern; dia gabisa berenti karena ga didefine kapan berentinya
rotabc [] = [] -- kalo ditambah ini jadi bisa :D

rotabc' = map abc
    where abc 'a' = 'b'
          abc 'b' = 'c'
          abc 'c' = 'a'
          abc x = x

-- NOMOR 3
-- Definisikan fungsi last, dengan menerapkan point-free style. Fungsi last tersebut menerima sebuah 
-- list  dan mengembalikan elemen terakhir dari list tersebut.

last' = head . reverse -- reverse listnya, terus ambil headnya.