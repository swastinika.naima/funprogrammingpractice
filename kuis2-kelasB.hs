import Data.List 
import qualified Data.Char as Char

-- NOMOR 1
-- Given a sentence, define a function called capitalise which returns the same sentence with 
-- all words capitalised except the ones from a given list. (source: https://www.fer.unizg.hr/)

getWord word [] = (word, [])
getWord word (x:xs) | x == ' ' = (word, xs)
                    | x /= ' ' = getWord (word ++ [x]) xs 
-- returns first word in a sentence

splitter lw [] = lw
splitter lw inp = let (word, rest) = getWord "" inp
                    in splitter (lw ++ [word]) rest
-- returns a list of words in the sentence

upper exception word = if word `elem` exception
                       then word
                       else Char.toUpper (head word) : (tail word)
-- capitalizes word if word is not in exception list

combine [] = []
combine [a] = a
combine (a:xs) = a ++ " " ++ (combine xs)
-- combines words in a list to a sentence with space inbetween each element of list

capitalize :: String -> [String] -> String
capitalize sentence exception = let lw = splitter [] sentence
                                in combine (map (upper exception) lw)

capitalize' sentence exception = combine (map (upper exception) (splitter[] sentence))

capitalize'' :: [String] -> String -> String
capitalize'' exception = combine . (map (upper exception)) . (splitter[])
-- note: capitalize'' accepts input in this order: exception list, sentence

-- NOMOR 2
-- Write a definition of composition function (compose) similar to the compose/dot
-- operator ( • ) in Haskell prelude, that accept two functions and return a new
-- function. Define the type of the compose function. 
-- (modified from: https://www2.cs.arizona.edu/classes/cs372/spring14)

compose :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
compose a b = (\x -> a(b x))

-- NOMOR 3
-- Definisikan fungsi last, dengan hanya menggunakan foldr atau foldl. Fungsi last
-- tersebut menerima sebuah list dan mengembalikan elemen terakhir dari list
-- tersebut.

last' :: [a] -> a
last' = foldl (\_ x -> x) undefined

-- NOMOR 4
-- Sebagaimana materi kuliah terkait Composing Contract, diperlihatkan sebuah
-- contract yang disebut Zero-Coupon Bound (zcb). Pemanggilan fungsi zcb t x k,
-- menyatakan bahwa pada waktu t, contract ini akan senilai dengan x pada kurs k .
-- Misalkan fungsi contract definisi lain sudah tersedia. Bagaimana mengkomposisikan-nya
-- untuk mendefinisikan fungsi zcb. Fungsi yang boleh anda gunakan adalah
-- antara lain: (when, give, and, or, at, scale, konst, one, zero)

-- zcb :: Date -> Double -> Currency -> Contract
-- zcb t x k = at t (scale x (one k)) ????????